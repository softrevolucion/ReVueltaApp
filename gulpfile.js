var gulp = require('gulp');
var less = require('gulp-less');
var browserSync = require('browser-sync').create();
var header = require('gulp-header');
var cleanCSS = require('gulp-clean-css');
var rename = require("gulp-rename");
var terser = require('gulp-terser');
var pkg = require('./package.json');

// Set the banner content
var banner = ['/*!\n',
    ' * <%= pkg.displayName %> v<%= pkg.version %> (<%= pkg.homepage %>)\n',
    ' * Copyleft 2018 - ' + (new Date()).getFullYear(), ' <%= pkg.author %>\n',
    ' * Licensed under <%= pkg.license %> (<%= pkg.licenseurl %>)\n',
    ' */\n',
    ''
].join('');

// Procesa los LESS a CSS
gulp.task('css', function() {
    return gulp.src('www/less/revuelta-app.less')
        .pipe(less())
        .pipe(cleanCSS({ compatibility: 'ie8' }))
        .pipe(header(banner, { pkg: pkg }))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('www/css'))
        .pipe(browserSync.reload({
            stream: true
        }))
});

// Proceso los JS
gulp.task('js', function() {
    return gulp.src('www/js/revuelta-app.js')
        .pipe(terser())
        .pipe(header(banner, { pkg: pkg }))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('www/js'))
        .pipe(browserSync.reload({
            stream: true
        }))
});


// Configure the browserSync task
gulp.task('browserSync', function() {
    browserSync.init({
        server: {
            baseDir: 'www/'
        },
    })
})

// Run everything
gulp.task('default', ['css', 'js']);


// Dev task with browserSync
gulp.task('dev', ['browserSync', 'css', 'js'], function() {
    gulp.watch('www/less/*.less', ['css']);
    gulp.watch('www/js/*.js', ['js']);
    // Reloads the browser whenever HTML or JS files change
    // gulp.watch('www/*.html', browserSync.reload);
    gulp.watch('www/js/**/*.js', browserSync.reload);
});
