/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

const ARTIST_DEFAULT = "[Album desconocido]",
	TITLE_DEFAULT = "[Track desconocido]",
	ALBUM_DEFAULT = "[Album desconocido]";

var img, live, artist = ARTIST_DEFAULT,
	title = TITLE_DEFAULT,
	album = ALBUM_DEFAULT,
    error_lastfm = false;

var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
        $(document).ready(function(){
            $('.sidenav').sidenav();
        });
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent("deviceready")
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        console.log("Received Event: " + id)
    },

    mostrarSeccion: function(pagina, disparador) {
        $(".pagina").hide();
        $(".active").removeClass("active");
        $("#" + pagina).show("fast");
        disparador.parentElement.className = disparador.parentElement.className + " active"
    }
};

var player = {
    // Player Constructor
    initialize: function() {
        setTimeout(this.loadMetadata, 2000);
        // Actualización de la información cada 20 segundos
        setInterval(this.loadMetadata, 20000);

        $('.reproductor-play').on('click', function() {
            var reproductor = document.getElementById('reproductor')
            if(reproductor.paused) {
                reproductor.play();
                $('.reproductor-play > i').html("pause");
            } else {
                reproductor.pause();
                $('.reproductor-play > i').html("play_arrow");
            }
        });
    },

    getAlbumInfo: function() {
       if(artist){
            $.ajax({
                type : 'POST',
                url : 'https://ws.audioscrobbler.com/2.0/',
                data: {
                    'method': 'track.getinfo',
                    'artist': artist,
                    'track': title,
                    'api_key': 'cbf693fb1f97713d1e30c22364d80ca0',
                    'format': 'json',
                },
                dataType : 'json',
                success : function(data) {
                    if (data.track && data.track.album) {
                        album = data.track.album.title;
                        img = data.track.album.image[3]['#text'];
                    } else {
                        album = ALBUM_DEFAULT;
                        img = undefined;
                    }
                    error_lastfm = false;;
                },
                error : function(data) {
                    album = ALBUM_DEFAULT;
                    img = undefined;
                    M.toast({html: 'No fue posible conectar con last.fm'});
                    error_lastfm = true;
                },
                complete: player.updateTrackInfo
            });
        }
    },

    updateTrackInfo: function() {
        var portrait = document.getElementById("portrait");

        document.getElementById("artist").innerHTML = artist;
        document.getElementById("title").innerHTML = title;
        document.getElementById("album").innerHTML = album;
        if(live) {
            document.getElementById("live").innerHTML = live;
        }

        if (img) {
            portrait.src = img;
            portrait.setAttribute("class", "album-cover");
        } else {
            portrait.src = "img/Audio-CD-icon.png";
            portrait.setAttribute("class", "rotated-image");
        }
    },

    loadMetadata: function() {
        //Obtener info canción
        $.ajax({
            //url: "http://giss.tv:8000/status-json.xsl?mount=/redlinux-cc.ogg"
            url: "https://www.revuelta.org/cancion.json",
            dataType: "json",
            success: function(t) {
                if("LIVE" == title.split("=")[0]) {
                    live = '<img src="https://www.revuelta.org/images/live.gif"> ';
                    var json = JSON.parse(title.split("=")[1]);
                    artist = json.artist;
                    title = json.title
                } else {
                    artist = t.icestats.source.artist, title = t.icestats.source.title;
                }
                //Llama a LastFM solo si cambia el titulo de la canción
                var titlehtml = document.getElementById("title").innerHTML;
                if(titlehtml !== title || error_lastfm) {
                    player.getAlbumInfo();
                    
                }
            },
            error: function(t) {
                artist = ARTIST_DEFAULT;
                title = TITLE_DEFAULT;
                album = ALBUM_DEFAULT;
                img = void 0;
                M.toast({
                    html: "No fue posible conectar con los servidores"
                })
            }
        })
    }
};

app.initialize();
player.initialize();
