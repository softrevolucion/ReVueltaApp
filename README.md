# Re-Vuelta Radio APP
[![version][version-badge]][changelog]

Re-Vuelta Radio APP es la aplicación oficial de [Re-Vuelta Radio](https://www.re-vuelta.org) para dispositivos móviles.  Mediante Re-Vuelta Radio APP puedes escuchar el streaming en directo de [Re-Vuelta Radio](https://www.re-vuelta.org) desde donde te encuentres.

## Desarrollo
### Dependencias
Este proyecto depende de las siguientes tecnologías/librerías/proyectos:

- Apache Cordova 8.0.0+
- Java Development Toolkit (JDK) 1.8+
- Android Software Development Toolkit para Android 4.4 (API 19)
- Gradle 2.0+
- GIT 2.7+
- Node Package Manager (NPM) 3.0+

### Configuración
Para poder empezar a desarrollar en este proyecto siga los siguientes pasos:

 1. Instale un entorno integrado de desarrollo (IDE) que incluya GIT, o bien, si va a utilizar únicamente un editor de texto, sólo instale GIT en su sistema operativo.
 2. Descargue/clone el repositorio de **ReVueltaApp** mediante el comando `git clone git@gitlab.com:softrevolucion/ReVueltaApp.git` en el directorio que usted defina como base de trabajo.  Puede hacer esta operación también mediante las herramientas de su IDE.
 3. Instale cordova mediante el comando `sudo npm install -g cordova` (para esto se requiere npm. Si no tiene instalado puede hacerlo desde el gestor de paquetes de su sistema operativo).
 4. Muévase al directorio `ReVueltaApp` y agregue la(s) plataforma(s) en las cuales desea desplegar la aplicación.  Por ejemplo, para android ejecute el comando `cordova platform add android`.
 5. Verifique que sus sitema cumple todos los requerimientos `cordova requirements`.  En caso de que no cumpla con alguno, por favor instálelo.
 6. Ejecute la aplicación.  Puede hacerlo de las siguientes formas: lo puede ejecutar en un emulador mediante `cordova emulate android` o `cordova emulate browser`, etc.  Lo puede ejecutar en su dispositivo móvil con `cordova run android`.
 7. Puede empezar a desarrollar sobre el código de este repositorio.  Sólo siga las políticas enumeradas en la sección de políticas.

### Políticas de desarrollo
Al realizar cualquier contribución a este proyecto, por favor acójase a las siguientes políticas:

- Cree una nueva rama por cada funcionalidad nueva que se deba desarrollar.
- Recuerde mezclar periódicamente hacia su rama los cambios que otros desarrolladores han cargado a la rama `development`; de esta manera reducirá el riesgo de conflictos cuando se vaya a mezclar su rama hacia la rama `development`.
- Sólo mezcle las ramas de funcionalidades con la rama `development` cuando haya terminado la funcionalidad completamente y esté lista para ser probada y utilizada por otros/as desarrolladores/as.  Consúltelo con la comunidad.
- NUNCA mezcle ninguna rama hacia las ramas `staging`y `master`. Esas ramas son responsabilidad del/la líder técnico y/o del personal de DevOps, encargados de los despliegues a los ambientes de pruebas/staging y de producción.
- Las correcciones de errores pueden ameritar una rama nueva o no (respecto a `development`) dependiendo de la magnitud de la implementación de la solución. Consúltelo con la comunidad.
- Toda implementación de correcciones de errores obliga a subir un dígito en la tercera posición del esquema de versionamiento. Así, por ejemplo, nuevas correcciones de errores implementadas sobre la versión `1.0.5` dan lugar a la versión `1.0.6`.
- Toda funcionalidad nueva (o conjunto de funcionalidades) obliga a subir un dígito en la segunda posición del esquema de versionamiento. Así, por ejemplo, nuevas funcionalidades creadas sobre la versión `1.0.5` dan lugar a la versión `1.1.0`.
- Toda gran cambio que genere alteraciones en el API que impidan que sea posible utilizar la mayoría de funcionalidades de la versión anterior obliga a subir un dígito en la primer posición del esquema de versionamiento. Así, por ejemplo, grandes cambios implementados sobre la versión `1.0.5` dan lugar a la versión `2.0.0`.
- Documente todo lo que desarrolla, cambia o corrige. Utilice los comentarios necesarios en el código fuente, en archivos de guía como este y en la documentación formal del proyecto (Manuales de usuario, técnicos o de despliegue/instalación). Entienda que si no documenta lo que hace, cualquier desarrollador/a en el futuro podría querer cortarse las venas al no entender la intención de lo que usted implementó hoy...y ese desarrollador/a podría ser usted mismo.
- Pregunte cuando lo necesite: otro/a desarrollador/a podría tener en cinco minutos la respuesta que a usted le podría costar horas en encontrar; el sol brilla fulgurante en el atardecer como para que usted se lo pierda por falta de comunicación con sus parceros/as.
- Sea limpio/a y elegante en la implementación de su código: otros/as desarrolladores/as lo podrán ver y se burlarán de usted si lo que implementa es mediocre. Somos implacables.

[changelog]: ./CHANGELOG.md
[version-badge]: https://img.shields.io/badge/version-0.1.3-green.svg
