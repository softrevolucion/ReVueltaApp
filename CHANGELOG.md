# Changelog
Todos los cambios destacables a este proyecto serán documentados en esta archivo.

El formato está basado en [Mantenga un Changelog](http://keepachangelog.com/es-ES/1.0.0/)
y este proyecto se adhiere al [Versionamiento semántico](https://semver.org/lang/es/) (Semantic Versioning).

## [Unreleased]
- Pendientes

## 0.1.2
### Bugs
- Debido a un cambio en last.fm se cambia datatype de jsonp a json.

## 0.1.1
### Modificated
- Cambio URLs por nuevo dominio.

## 0.1.0
### Added
- Soporte para reproducción de streaming de ReVuelta Radio.
